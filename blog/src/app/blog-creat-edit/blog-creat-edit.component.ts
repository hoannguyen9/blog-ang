import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiBlogService } from '../api-blog.service';
import { Category, Postions } from "../mock-data/index";

@Component({
  selector: 'app-blog-creat-edit',
  templateUrl: './blog-creat-edit.component.html',
  styleUrls: ['./blog-creat-edit.component.scss']
})
export class BlogCreatEditComponent implements OnInit {
  public id!: number
  blog = {
    category: '0',
    public: false,
    position: [1],
    title: '',
    des: '',
    data_pubblic: '',
    thumbs: '',
    detail: ''
  };
  selectedValue = null
  category = Category
  listPosition = Postions

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private apiBlog: ApiBlogService
    ){
      this.route.params.subscribe(
        params => {
          this.id = params['id']
        }
      )
  }

  ngOnInit(): void {
    if (this.id) {
      this.getBlogById(this.id)
    }
  }

  /**
   * get blog by id
   *
   * @param id
   */
  getBlogById(id:number) {
    this.apiBlog.getBlogById(id).subscribe(
      (data) => {
        this.blog = data
      }
    )
  }

  /**
   * set position
   *
   * @param e
   * @param id
   * @returns
   */
  setPosition(e: any, id: number) {
    if (e.target.checked) {
      this.blog.position.push(id)
    }else {
      this.blog.position = this.blog.position.filter(res => res !== id)
    }
    return this.blog.position
  }

  /**
   * Submit Form
   */
  onSave() {
    if (this.id) {
      this.apiBlog.updateBlog(this.blog, this.id).subscribe(
        () => {
          console.log('success');
          this.router.navigate(['/list'])
        }
      )
    }else{
      this.apiBlog.onCreateBlog(this.blog).subscribe(
        () => {
          this.router.navigate(['/list'])
        }
      )
    }
  }

  /**
   * Clear Form
   */
  onClear() {
    this.blog = {
      category: '0',
      public: false,
      position: [1],
      title: '',
      des: '',
      data_pubblic: '',
      thumbs: '',
      detail: ''
    }
  }
}
