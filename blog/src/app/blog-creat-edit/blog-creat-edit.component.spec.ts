import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BlogCreatEditComponent } from './blog-creat-edit.component';

describe('BlogCreatEditComponent', () => {
  let component: BlogCreatEditComponent;
  let fixture: ComponentFixture<BlogCreatEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BlogCreatEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BlogCreatEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
