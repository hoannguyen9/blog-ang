export var Postions: any = [
  {
    name: "Việt Nam",
    id: 1
  },
  {
    name: "Châu Á",
    id: 2
  },
  {
    name: "Châu Âu",
    id: 3
  },
  {
    name: "Châu Mỹ",
    id: 4
  },
]

export var Category: any = [
  { name: 'Tổng hợp', id: 0},
  { name: 'Thời sự', id: 1},
  { name: 'Tạp chí', id: 2},
  { name: 'Thể thao', id: 3},
  { name: 'Thời tiết', id: 4},
  { name: 'Nhịp sống', id: 5},
  { name: 'Báo đài', id: 6},
  { name: 'Hình sự', id: 7},
  { name: 'Phim ảnh', id: 8},
  { name: 'Thời trang', id: 9},
  { name: 'Âm nhạc', id: 10},
  { name: 'Bóng đá', id: 11},
  { name: 'Sống khỏe', id: 12},
]
