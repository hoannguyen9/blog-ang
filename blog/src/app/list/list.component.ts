import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { List } from 'src/model/list';
import { ApiBlogService } from "../api-blog.service";
import { Postions, Category } from "../mock-data/index";

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class ListComponent implements OnInit {
  public itemSearch!: string;
  public pathUrl:string | undefined
  lists:List[] | undefined

  constructor(private route: Router, private apiBlog: ApiBlogService) {
    route.events.subscribe();
    this.pathUrl = this.route.url
  }

  ngOnInit(): void {
    this.getBlog()
  }

  /**
   * get all blog
   */
  getBlog(): void {
    // this.lists = this.apiBlog.getListBlog()
    this.apiBlog.getListBlog().subscribe(
      (data) => {
        this.lists = data
      }
    )
  }

  /**
   * convert position
   *
   * @param position
   */
  convertPosition(position: any) {
    let namePosition: any[] = []
    if (position.length > 1) {
      position.forEach((id: any) => {
        const name = Postions.find((el:any)=> {
         return el.id == id
        }).name
        namePosition.push(name)
      });
      const result = namePosition.join(',\n')
      return result
    }
    namePosition = Postions.find((el:any) => el.id == position[0]).name
    return namePosition
  }

  /**
   * convert status
   *
   * @param status
   */
  convertCategory(category: string) {
    const result =  Category.find((el:any) => el.id == category)
    if (result) {
      return result.name
    }
    return 'Khác'
  }

  /**
   * delete blog
   *
   * @param id
   */
  onDelete(id: number){
    this.apiBlog.onDeleteBlog(id).subscribe(
      () => {
        this.getBlog()
      }
    )
  }

  /**
   * Search blog
   *
   * @param title
   */
  onSearch() {
    console.log('data', this.itemSearch);
    this.apiBlog.getBlogByCondition(this.itemSearch).subscribe(
      (data) => {
        this.lists = data
      }
    )
  }
}
