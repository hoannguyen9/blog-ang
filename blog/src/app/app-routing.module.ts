import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BlogCreatEditComponent } from './blog-creat-edit/blog-creat-edit.component';
import { ListComponent } from './list/list.component';
const routes: Routes = [
  { path: '', redirectTo: '/list', pathMatch: 'full' },
  { path: 'search', component: ListComponent },
  { path: 'new', component: BlogCreatEditComponent },
  { path: 'edit/:id', component: BlogCreatEditComponent, pathMatch: 'full' },
  { path: 'list', component: ListComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {  }
