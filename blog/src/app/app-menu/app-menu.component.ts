import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-app-menu',
  templateUrl: './app-menu.component.html',
  styleUrls: ['./app-menu.component.scss']
})
export class AppMenuComponent implements OnInit {
  id!: number

  constructor() {
    this.id = 1
  }
  ngOnInit(): void {
  }

  addClass(id: number) {
    this.id = id
  }
}
