import { HttpClient, HttpHeaders, HttpParams  } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, tap } from 'rxjs/operators';
import { List } from 'src/model/list';
import { Observable, of } from 'rxjs';

const httpOption = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    Authorization: 'my-auth-token'
  })
}

@Injectable({
  providedIn: 'root'
})
export class ApiBlogService {
  constructor(private http: HttpClient) { }
  private BaseApi = 'http://localhost:3000/blogs/';


  /**
   * Get all blog
   *
   * @returns Array
   */
  getListBlog(): Observable<List[]> {
    return this.http.get<List[]>(this.BaseApi).pipe(
      tap((data) => {
        data.sort((a, b) => b.id-a.id)
      }),
      catchError((error: any) => {
        console.log(error);
        return of([])
      })
    );
  }

  /**
   * get blog by condition
   *
   * @param param
   * @returns
   */
  getBlogByCondition(param: string): Observable<List[]> {
    if (!param) {
      return this.getListBlog()
    }
    return this.http.get<List[]>(this.BaseApi + '?title_like=' + param).pipe(
      tap((data) => {
        data.sort((a, b) => b.id-a.id)
      }),
      catchError(() => of([]))
    );
  }

  /**
   * Get blog by Id
   *
   * @param id
   * @returns
   */
  getBlogById(id:number): Observable<List> {
    return this.http.get<List>(this.BaseApi + id).pipe(
      tap(_ => console.log('sucess')),
      catchError(() => of(new List()))
    );
  }

  /**
   * Creat new Blog
   *
   * @param data
   * @returns
   */
  onCreateBlog(data: any): Observable<List> {
    return this.http.post<List>(this.BaseApi, data, httpOption).pipe(
      tap(_ => console.log('sucess')),
      catchError((error: any) => {
        console.log(error);
        return of(data)
      }))
  }

  /**
   * update Blog
   *
   * @param data
   * @param id
   * @returns
   */
  updateBlog(data: any, id: number): Observable<List> {
    return this.http.put<List>(this.BaseApi + id, data, httpOption).pipe(
      catchError((error: any) => {
        console.log(error);
        return of(data)
      }))
  }

  /**
   * Delete blog
   *
   * @param id
   * @returns
   */
  onDeleteBlog(id: number): Observable<List> {
    return this.http.delete<List>(this.BaseApi + id).pipe(
      tap(_ => console.log('sucess')),
      catchError((error: any) => {
        console.log(error);
        return of(new List)
      })
    );
  }
}
